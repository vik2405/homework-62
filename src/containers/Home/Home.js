import React, { Component } from 'react';
class Home extends Component {
    render() {
        return (
            <div className="home">
                <h1>Home</h1>
                <div className="bigphoto3"><img src="img/photo2.jpeg" alt=""/></div>
            </div>
        );
    }
}

export default Home;

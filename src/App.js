import React, {Component} from 'react';
import './App.css';
import Route from "react-router-dom/es/Route";
import About from "./containers/About us/About us";
import Contacts from "./containers/Contacts/Contacts";
import Switch from "react-router-dom/es/Switch";

import Header from "./components/Header/Header";
import Footer from "./components/Footer";
import Home from "./containers/Home/Home";

class App extends Component {
    render() {
        return (
            <div className="App">
                <Header/>
                <Switch>
                    <Route path="/" exact component={Home}/>
                    <Route path="/about" component={About}/>
                    <Route path="/contacts" component={Contacts}/>
                </Switch>
                <Footer/>
            </div>
        );
    }
}

export default App;

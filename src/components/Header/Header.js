import React from 'react';
import {NavLink} from "react-router-dom";
import './Header.css';
const Header = () => {
    return (
        <header>
            <nav>
                <ul className="ul">
                    <li className="li"><NavLink to="/" exact activeClassName="active">Home</NavLink></li>
                    <li><NavLink to="/about" activeClassName="active">About us</NavLink></li>
                    <li><NavLink to="/contacts" activeClassName="active">Contacts</NavLink></li>
                </ul>
            </nav>
        </header>
    )
};

export default Header;
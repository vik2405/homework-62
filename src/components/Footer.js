import React, {Component} from 'react';
function Footer() {
    return (
        <div className="footer">
            <div className="container">
                <div className="logotype">
                    <p>Stay in touch</p>
                    <div className="references">
                        <a href="#" className="facebook"></a>
                        <a href="#" className="twitter"></a>
                        <a href="#" className="youtube"></a>
                        <a href="#" className="instagram"></a>
                    </div>
                </div>
                <h4>Resources</h4>
                <div className="text2">Easily manage your inspiration and work-in-progress by dragging images into
                    projects and sharable client groups. work-in-progress by dragging images into projects and sharable
                    client group
                </div>
            </div>
        </div>
    )
}
export default Footer;